import network
import time
import bmp280
import urequests
from machine import Pin, I2C, ADC
import sys
def init_sensor():
    #初始化传感器
    global switch, adc, pin_a, pin_b, position, bmp, led
    
    #初始化开关量
    switch = Pin(23, Pin.IN, Pin.PULL_UP) #将23管脚设置为开关量输入，并设置上拉，默认为高电平，开关接通变为低电平
    
    #初始化电压读取
    pin = Pin(32, Pin.IN) #管脚32作为电压测量输入
    adc = ADC(pin) #初始化模数转换       
    adc.atten(ADC.ATTN_2_5DB) #指定输入信号衰减量，测量范围是100mV - 1250mV
       
    #初始化旋转编码器
    position = 0 #初始化编码器位置
    pin_a = Pin(25, Pin.IN)#36旋转编码器输入，不行
    pin_b = Pin(26, Pin.IN)#39旋转编码器输入，不行
    # 设置编码器中断处理函数，在A管脚上升沿触发中断
    pin_a.irq(trigger=Pin.IRQ_RISING, handler=coder_handle_interrupt)
    
    #初始化气压和温度传感器bmp280
    bus = I2C(1, scl=Pin(12), sda=Pin(15)) #初始化i2c总线
    bmp = bmp280.BMP280(bus) #初始化压力和温度传感器
    
    #初始化led
    led = Pin(22, Pin.OUT) # 把22管脚设置为输出

def coder_handle_interrupt(pin):
    #旋转编码器中断函数，当A信号上升沿触发
    global position, pin_a, pin_b #使用前先声明全局变量，不然就会认为是局部变量
    
    value_a = pin_a.value() #读取管脚A的值
    value_b = pin_b.value() #读取管脚B的值
    
    # 根据A和B引脚的状态判断旋转方向
    p = position
    if (value_a==1) and (value_a == value_b): ## AB同为高电平正向+1
        position += 1
    if (value_a==0) and (value_a != value_b): ## A为低电平且B为高电平-1
        position -= 1
    if p!=position:
        #position发生变化时打印信息
        print("pin:", pin, "a:",value_a, "b:",value_b, "position:", position)
    
def get_data():
    #读取各个传感器的数据并返回JSON格式
    global bmp, position, adc, switch
    data = {"switch": switch.value(),
            "voltage": 0.207+(adc.read()-405)*0.31/1000, #根据量程进行转换
            "temperature":bmp.getTemp(),
            "pressure":bmp.getPress(),
            "altitude":bmp.getAltitude(),
            "position":position}
    return data

    
def connect_wifi(ssid, pwd):
    #连接到WiFi网络
    try:
        print('尝试连接到WiFi'+ssid)
        wlan = network.WLAN(network.STA_IF) # 创建站点接口
        wlan.active(False)
        wlan.active(True)                   # 激活接口
        if not wlan.isconnected():          # 检查是否连接 
            wlan.connect(ssid, pwd)    # 连接到热点
            i = 0
            while not wlan.isconnected():   # 检查状态直到连接到热点或失败
                time.sleep(1) #等待1秒
                print('正在连接',ssid,i)
                i = i+1
                if i>=10: #大于10秒超时，返回
                    print('连接WiFi热点%s失败'%(ssid))   
                    return None
        print('已经连接到WiFi%s'%(ssid))        
        print('网络信息:', wlan.ifconfig()) #打印网络信息
        return wlan.ifconfig() #连接成功后返回网络信息
    except Exception as e:
        print(e)
        return None

def verify_device(server, key):
    #验证设备的key并获取token，相当于设备登录
    url = server+'/device/verify'
    data = {'key': key}
    headers = {'Content-Type': 'application/json'}
    try:
        print(url)
        response = urequests.post(url, json=data, headers=headers)
        result = response.json()
        response.close()
        if result.get('code')==0:
            print('验证Key通过', result)
            return result.get('token')
        else:
            print('验证设备Key未通过', result)
        return None
    except Exception as e:
        sys.print_exception(e)
        print('验证设备Key时出错:', e)
        return None

def set_led_state(state: bool):
    if state==True:
        led.value(0)     #打开LED，这个LED是低电位点亮
    else:
        led.value(1)

def heart_beat(server, token):
    #心跳，提交数据
    url = server + '/device/beat'
    headers = {'token': token}
    data = get_data()
    try:
        print("数据：", data)
        response = urequests.post(url, json=data, headers=headers)
        result = response.json()
        response.close()
        if result.get('code')==0:
            print('心跳正常', result)
            set_led_state(result.get('led_state'))
        else:
            print('心跳异常', result)
        return result
    except Exception as e:
        print('Error:', e)
        return None

def main():
    #主函数
    #连接到WiFi网络
    server = 'http://your cumputer ip or server url'
    key = 'Y3nU95sU'
    ap_ssid = 'your wifi'
    ap_pwd = 'your wifi password'
    ipcfg= connect_wifi(ap_ssid, ap_pwd)
    if ipcfg:
        token = verify_device(server, key)
        if token:
            #初始化传感器
            init_sensor()
            while True:
                heart_beat(server, token)
                time.sleep(2)

main() #执行主函数
