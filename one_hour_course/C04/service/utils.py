import time
import hashlib

def milliseconds(t=None):
    if t==None:
        t = time.time()
    return int(round(t * 1000))

def seconds(t=None):
    if t==None:
        t = time.time()
    return int(round(t))

def now_str(fmt="%Y-%m-%d %H:%M:%S"):
    return time.strftime(fmt, time.localtime())

def md5_string(s):
    m = hashlib.md5()
    m.update(s.encode('utf-8'))
    return m.hexdigest()

