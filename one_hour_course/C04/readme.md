# 1小时入门Python物联网开发04-提供服务，最简单的服务器

## 使用步骤

### 服务端

* 服务端代码使用thonny作为开发工具
* 其中：
    1. `app.py`：封装了API
    2. `services.py`：封装了业务逻辑
    3. `dao.py`：实现了数据库的访问
    4. `utils.py`：实现了一些常用的函数
    5. `login.html`：为登录的静态页面
    6. `main.html`：为管理端的主界面，使用到了jquery
* 要正常运行和编译，需要安装flask包，并将上述文件放在同一个文件夹下
* 使用下面的脚本来对数据库进行初始化，添加一个设备和用户：
``` javascript
db.users.insertOne({"name" : "test", "pwd" : "cf79ae6addba60ad018347359bd144d2" })
db.devices.insertOne({ "key" : "Y3nU95sU" })
```

### 设备端
* 使用选项中的解释器页签，安装MicroPython 固件：[ESP32_GENERIC-20240222-v1.22.2.bin](https://micropython.org/resources/firmware/ESP32_GENERIC-20240222-v1.22.2.bin)
* 将main.py、bmp280.py另存到开发板
* 使用包管理功能，搜索并安装urequests包
* 可以在ide环境直接运行main.py或者直接上传后运行

## 硬件

* ESP32开发板，芯片型号：ESP32-D0WDQ6，板载4MFlash
* 旋转编码器：EC11
* 大气压力和温度传感器：BMP280
* 开发板接线图![电路图](https://gitee.com/sunatom/python-iot-development/raw/master/one_hour_course/C03/%E7%94%B5%E8%B7%AF%E5%9B%BE.png)

## 注意事项：

* ESP32的ADC模块管脚的问题，第2个数模转换模块与WiFi共用，所以在使用WiFi的时候0,2,4,12-15以及25-27管脚不能用于ADC
* ESP32的ADC电压量程问题，需要根据情况进行相应的衰减，不同的衰减测量范围也不同，具体可参见本人博客文章：[MicroPython ESP32 ADC电压测量问题](https://blog.csdn.net/superatom01/article/details/134672562)
* ESP32的运行内存有限，在使用WiFi模块时，如果使用的第三方包占用内存过多，可能会导致网络初始化失败或者其他不明确的错误，如果你使用github上另外一个[bmp280的包](https://github.com/dafvid/micropython-bmp280)，极有可能会导致WiFi模块初始化失败。这个例子中使用的是[这个包](https://github.com/micropython-Chinese-Community/mpy-lib)，功能没有那么强大，但是不会出现内存不足导致的网络初始化失败