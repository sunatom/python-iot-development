import network
import time
import bmp280
from microdot import Microdot, Response, send_file
from machine import Pin, I2C, ADC

def init_sensor():
    #初始化传感器
    global switch, adc, pin_a, pin_b, position, bmp
    
    #初始化开关量
    switch = Pin(23, Pin.IN, Pin.PULL_UP) #将23管脚设置为开关量输入，并设置上拉，默认为高电平，开关接通变为低电平
    
    #初始化电压读取
    pin = Pin(32, Pin.IN) #管脚32作为电压测量输入
    adc = ADC(pin) #初始化模数转换       
    adc.atten(ADC.ATTN_2_5DB) #指定输入信号衰减量，测量范围是100mV - 1250mV
       
    #初始化旋转编码器
    position = 0 #初始化编码器位置
    pin_a = Pin(25, Pin.IN)#36旋转编码器输入，不行
    pin_b = Pin(26, Pin.IN)#39旋转编码器输入，不行
    # 设置编码器中断处理函数，在A管脚上升沿触发中断
    pin_a.irq(trigger=Pin.IRQ_RISING, handler=coder_handle_interrupt)
    
    #初始化气压和温度传感器bmp280
    bus = I2C(1, scl=Pin(12), sda=Pin(15)) #初始化i2c总线
    bmp = bmp280.BMP280(bus) #初始化压力和温度传感器


def coder_handle_interrupt(pin):
    #旋转编码器中断函数，当A信号上升沿触发
    global position, pin_a, pin_b #使用前先声明全局变量，不然就会认为是局部变量
    
    value_a = pin_a.value() #读取管脚A的值
    value_b = pin_b.value() #读取管脚B的值
    
    # 根据A和B引脚的状态判断旋转方向
    p = position
    if (value_a==1) and (value_a == value_b): ## AB同为高电平正向+1
        position += 1
    if (value_a==0) and (value_a != value_b): ## A为低电平且B为高电平-1
        position -= 1
    if p!=position:
        #position发生变化时打印信息
        print("pin:", pin, "a:",value_a, "b:",value_b, "position:", position)
    
def get_data():
    #读取各个传感器的数据并返回JSON格式
    global bmp, position, adc, switch
    data = {"switch": switch.value(),
            "voltage": 0.207+(adc.read()-405)*0.31/1000, #根据量程进行转换
            "temperature":bmp.getTemp(),
            "pressure":bmp.getPress(),
            "altitude":bmp.getAltitude(),
            "position":position}
    return data

    
def connect_wifi(ssid, pwd):
    #连接到WiFi网络
    try:
        print('尝试连接到WiFi'+ssid)
        wlan = network.WLAN(network.STA_IF) # 创建站点接口
        wlan.active(False)
        wlan.active(True)                   # 激活接口
        if not wlan.isconnected():          # 检查是否连接 
            wlan.connect(ssid, pwd)    # 连接到热点
            i = 0
            while not wlan.isconnected():   # 检查状态直到连接到热点或失败
                time.sleep(1) #等待1秒
                print('正在连接',ssid,i)
                i = i+1
                if i>=10: #大于10秒超时，返回
                    print('连接WiFi热点%s失败'%(ssid))   
                    return None
        print('已经连接到WiFi%s'%(ssid))        
        print('网络信息:', wlan.ifconfig()) #打印网络信息
        return wlan.ifconfig() #连接成功后返回网络信息
    except Exception as e:
        print(e)
        return None

def start_ap():
    #启动WiFi热点
    print('正在启动wifi热点：ESP32-WiFi')
    ap = network.WLAN(network.AP_IF) # 创建热点接口
    ap.config(essid='ESP32-WiFi') # 指定热点名称
    ap.active(True)         # 激活
    print('wifi热点：ESP32-WiFi 已启动...')
    
def start_server(ip):
    #启动web服务
    app = Microdot()# 创建Web服务
    @app.route('/')  #绑定缺省页面的路由
    def index_page(request):
        #print('/ 配置页面请求，返回index.html')
        return send_file('index.html')
    
    @app.route('/getdata')  #绑定返回数据的路由
    def get_ap_list(request):
        data = get_data()
        #print('/getdata 请求，返回', data)
        return Response(body=data) #返回Json数据

    # 打印服务的地址
    print("服务已经启动，请使用浏览器访问： http://%s "%(ip))
    app.run(host='0.0.0.0', port=80)
    
def main():
    #主函数
    #初始化传感器
    init_sensor()
    #连接到WiFi网络
    ipcfg= connect_wifi("wifi","password")
    if ipcfg:
        #启动Web服务
        start_server(ipcfg[0])

main() #执行主函数
