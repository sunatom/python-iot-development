import time
import hashlib

def milliseconds(t=None):
    if t==None:
        t = time.time()
    return int(round(t * 1000))

def seconds(t=None):
    if t==None:
        t = time.time()
    return int(round(t))

def now_str(fmt="%Y-%m-%d %H:%M:%S"):
    return time.strftime(fmt, time.localtime())

def rtc_now_date_time():
    t = time.localtime()
    #[year, month, day, weekday, hours, minutes, seconds, subseconds]
    return [t.tm_year, t.tm_mon, t.tm_mday, t.tm_wday, t.tm_hour, t.tm_min, t.tm_sec, 0]

def md5_string(s):
    m = hashlib.md5()
    m.update(s.encode('utf-8'))
    return m.hexdigest()

