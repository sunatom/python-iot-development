# 1小时入门Python物联网开发06-宠物智能喂食器（完结篇）

## 功能

使用ESP32和3D打印技术实现了一个宠物喂食器，主要是目的将之前课程学习的内容进行融会贯通，通过实际项目来感受物联网智能家居产品的开发。

主要功能有：
* 定时定量出粮，可以给喂食器设置出粮的时间和数量
* 语音呼唤，出粮时可以播放提前录制的声音，用于呼唤宠物进食，增进和宠物的情感连接
* 缺粮提醒，自动缺粮检测，并通过指示灯和应用状态来提醒缺粮
* 远程控制，可以远程对喂食器进行设置和管理


## 使用步骤

### [设备端](https://gitee.com/sunatom/python-iot-development/tree/84df65055ca97c4b21f157b28a82e81936d97f08/one_hour_course/C06/esp32)
* 使用选项中的解释器页签，安装MicroPython 固件：[ESP32_GENERIC-20240222-v1.22.2.bin](https://micropython.org/resources/firmware/ESP32_GENERIC-20240222-v1.22.2.bin)
* 将esp32文件夹下的文件上传到开发板根路径下
* 使用包管理功能，搜索并安装microdot和urequests包
* 可以在ide环境直接运行main.py或者直接上传后运行
* 设备端代码使用thonny作为开发工具

### [Web端](https://gitee.com/sunatom/python-iot-development/tree/84df65055ca97c4b21f157b28a82e81936d97f08/one_hour_course/C06/web)

* Web端使用[VSCode](https://code.visualstudio.com/)作为开发环境
* 使用了[Vue3.0](https://cn.vuejs.org/)的框架

### [服务端](https://gitee.com/sunatom/python-iot-development/tree/84df65055ca97c4b21f157b28a82e81936d97f08/one_hour_course/C06/iot_service)

* 服务端使用[VSCode](https://code.visualstudio.com/)作为开发环境
* 使用Flask作为框架
* 数据库使用MongoDB

## 硬件

* ESP32开发板，芯片型号：ESP32-D0WDQ6，板载4MFlash
* 红外对射模块
* 时钟模块
* 语音模块
* 电机驱动模块
* 直流减速电机
* 3.3/5V电源模块
* 开发板接线图![电路图](https://gitee.com/sunatom/python-iot-development/raw/84df65055ca97c4b21f157b28a82e81936d97f08/one_hour_course/C06/images/%E7%94%B5%E8%B7%AF%E5%9B%BE.png)

### 3d打印件

* 3d打印材料使用PLA，其他材料可能尺寸未必合适
* 如果需要原始设计文件可以给我留言
* 如果需要硬件的购买链接可以私信我，本人不出售硬件