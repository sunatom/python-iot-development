import { createRouter, createWebHistory } from 'vue-router';
import LoginView from '../views/LoginView.vue';
import HomeView from '../views/HomeView.vue';
import DeviceEditorView from '../views/DeviceEditorView.vue';
import FeederSettingView from '../views/FeederSettingView.vue';
import { IotApi } from '../js/apis';
import FeedingRecordView from '../views/FeedingRecordView.vue';
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta:{
        auth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView,
      meta:{
        auth: false
      }
    },
    {
      path: '/active-device/:id?/:device_id?',
      name: 'active_device',
      props: true,
      component: DeviceEditorView,
      meta:{
        auth: true
      }
    },
    {
      path: '/edit-pet-feeder-setting/:device_id?/:title?',
      name: 'edit_pet_feeder_setting',
      props: true,
      component: FeederSettingView,
      meta:{
        auth: true
      }
    },
    {
      path: '/feeding-record/:device_id?',
      name: 'feeding_record',
      props: true,
      component: FeedingRecordView,
      meta:{
        auth: true
      }
    }
  ]
});
router.beforeEach(async (to, from)=>{
  //检查是否是不需要验证权限的页面
  if(to.meta.auth==true){
    //验证权限
    if(! IotApi.logined() && to.name!='login'){
      return {name: 'login'};
    }
  }
});

export default router
