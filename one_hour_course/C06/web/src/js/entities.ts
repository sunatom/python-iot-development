export interface DeviceType{
    dtid: string; //设备类型id
    name: string; //设备类型名称
    icon_url: string; //设备图标的url
}

export interface DeviceInfo{
    id: string; //设备id
    user_id: string; //用户id
    name: string; //设备名称
    type: DeviceType; //设备类型
    key: string; //设备的key
    enable: boolean; //设别是否可用
    online: boolean; //设备是否在线
    empty: boolean; //设备是否缺粮
}

export interface FeederSetting{
    //喂食器出料设置
    time: string; //出料时间
    set: number;//出料份数
}

export interface FeedingRecord{
    //喂食记录
    time: string; //日期时间 yyyy-mm-dd hh:mm:ss
    set: number;  //出料份数 
}

export class Consts{
    static SENDER_DEVICE: string = 'device';
    static SENDER_USER: string = 'user';
}