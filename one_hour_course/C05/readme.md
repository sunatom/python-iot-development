# 1小时入门Python物联网开发05-执行动作，实现智能红外遥控器

## 功能

使用ESP32实现了一个在线虚拟遥控器，主要是目的是学习红外遥控的基本原理，掌握如何使用红外收发模块，对家电等设备进行远程控制，为实现完整的智能物联网应用打好基础做好铺垫。

主要功能有：
* 红外遥控信号学习，使用浏览器访问单片机web服务，按Ctrl+点击相应的按钮，进入学习模式
* 使用实体红外遥控器对准红外接收模块，按下相应的按键，完成学习
* 学习后的在线虚拟遥控器按键为蓝色字体
* 点击虚拟遥控器的按钮，可以对设备进行相应的控制

![功能图](https://gitee.com/sunatom/python-iot-development/raw/master/one_hour_course/C05/images/%E5%AE%9E%E7%8E%B0%E7%9A%84%E5%8A%9F%E8%83%BD.png)



## 使用步骤

### 设备端
* 使用选项中的解释器页签，安装MicroPython 固件：[ESP32_GENERIC-20240222-v1.22.2.bin](https://micropython.org/resources/firmware/ESP32_GENERIC-20240222-v1.22.2.bin)
* 将`main.py`、`irctrl.py`、`index.html`上传到开发板根路径下
* 使用包管理功能，搜索并安装microdot包
* 可以在ide环境直接运行main.py或者直接上传后运行，注意`irctrl.py`和`index.html`必须要先上传
* 设备端代码使用thonny作为开发工具
* 其中：
    1. `main.py`：主程序代码
    2. `irctrl.py`：封装了红外
    3. `index.html`：实现了一个虚拟遥控器的操作页面
    4. `cmds.json`：红外指令的存储文件，学习后会自动创建

## 硬件

* ESP32开发板，芯片型号：ESP32-D0WDQ6，板载4MFlash
* 红外接收模块
* 红外发射模块
* 开发板接线图![电路图](https://gitee.com/sunatom/python-iot-development/raw/master/one_hour_course/C05/images/%E7%94%B5%E8%B7%AF%E5%9B%BE.png)

## 注意事项：

* 如果使用大功率的红外发射模块，需要给模块单独供电
* 如果你要控制其他设备，可以修改index.html，给不同按钮分配不同的id，这样就可以用手机控制家里的不同设备了
* 涉及到的硬件都可以在某宝购买到，如果需要链接可私信（本人不卖硬件，用的东西也是直接在某宝买的）