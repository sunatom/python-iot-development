# Python物联网开发

#### 介绍
这是使用Python进行物联网开发系列课程的示例代码库，代码大都是以支持MicroPython的ESP32开发板调试通过，如果没有特别说明，使用的开发板都是ESP32，开发板如下图：![输入图片说明](one_hour_course/C01/esp32-1.png)

文件夹 one_hour_course 下是所有课程的源代码、电路图、设计文件、3D模型等。

课程在B站：

1小时入门Python物联网开发：

（1）MicroPython开发环境搭建    [教学视频](https://www.bilibili.com/video/BV1sz42197Z9/?share_source=copy_web&vd_source=850c0fa5185ae280f676ee3f51f97d53)    [源码](one_hour_course/C02)

（2）连接网络获取数据    [教学视频](https://www.bilibili.com/video/BV13Z421B7mC/?share_source=copy_web&vd_source=850c0fa5185ae280f676ee3f51f97d53)    [源码](one_hour_course/C02)

（3）获取传感器数据    [教学视频](https://www.bilibili.com/video/BV1xf421U7Nk/?share_source=copy_web&vd_source=850c0fa5185ae280f676ee3f51f97d53)    [源码](one_hour_course/C03)

（4）提供服务，最简单的服务器    [教学视频](https://www.bilibili.com/video/BV1jn4y1X7tY/?spm_id_from=333.999.0.0&vd_source=fd1c32bda6f43f30b5f57fb25bb787df)    [源码](one_hour_course/C04)

（5）执行动作，实现智能红外遥控器    [教学视频](https://www.bilibili.com/video/BV1yJ4m1T7vF/?share_source=copy_web&vd_source=850c0fa5185ae280f676ee3f51f97d53)    [源码](one_hour_course/C05)

（6）实现智能宠物喂食器 [教学视频](https://www.bilibili.com/video/BV1JayjYwEv8/?share_source=copy_web&vd_source=850c0fa5185ae280f676ee3f51f97d53)    [源码](one_hour_course/C06)
