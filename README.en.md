# Python Iot development

#### introduction

This repositories 
This is an example code library for the IoT development series courses using Python. The code is mostly debugged using an ESP32 development board that supports MicroPython. Unless otherwise specified, all development boards used are ESP32, as shown in the following figure: ![ESP32 board](one_hour_course/C01/esp32-1.png)


Courses on Bilibili：

1-hour introductory Python IoT development：

（1）MicroPython Development environment setup    [Video](https://www.bilibili.com/video/BV1sz42197Z9/?share_source=copy_web&vd_source=850c0fa5185ae280f676ee3f51f97d53)    [Souce Code](one_hour_course/C02)

（2）Connect to Internet and get data    [Video](https://www.bilibili.com/video/BV13Z421B7mC/?share_source=copy_web&vd_source=850c0fa5185ae280f676ee3f51f97d53)    [Souce Code](one_hour_course/C02)

（3）Obtain sensor data    [Video](https://www.bilibili.com/video/BV1xf421U7Nk/?share_source=copy_web&vd_source=850c0fa5185ae280f676ee3f51f97d53)    [Souce Code](one_hour_course/C03)

（4）Providing services, the simplest server    [Video](https://www.bilibili.com/video/BV1jn4y1X7tY/?spm_id_from=333.999.0.0&vd_source=fd1c32bda6f43f30b5f57fb25bb787df)    [Souce Code](one_hour_course/C04)

（5）Execute actions to achieve intelligent infrared remote control    [Video](https://www.bilibili.com/video/BV1yJ4m1T7vF/?share_source=copy_web&vd_source=850c0fa5185ae280f676ee3f51f97d53)    [Souce Code](one_hour_course/C05)

